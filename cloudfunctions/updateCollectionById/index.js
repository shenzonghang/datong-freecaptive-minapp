const upd_freeCaptiveDetails = require('./src/upd_freeCaptiveDetails')
const upd_kindPersonInfo = require('./src/upd_kindPersonInfo')
const upd_kindPersonList = require('./src/upd_kindPersonList')

/**根据ID更新放生明细表数据，更改金额时用到 */
exports.main = async (event) => {
  console.log('freeCap的值为：',event)
  const collectionName = event.collectionName
  switch(collectionName){
    case 'kindPersonInfo':
      return await upd_kindPersonInfo.main(event)
    case 'kindPersonList':
      return await upd_kindPersonList.main(event)
    case 'freeCaptiveDetails':
      return await upd_freeCaptiveDetails.main(event)
    default:
      console.error('标记位不符合规则.')
      return {data: undefined, errMsg: '集合不存在.'}
  }
}