const cloud = require('wx-server-sdk')
cloud.init({env: 'evn-cloud-a-4gf1q6iy9df62d73'})
const db = cloud.database()
const _ = db.command

/**根据手机号更新功德主名 */
const updateMeriNameById = async (data) => {
  const _id = data.id
  const meriName = data.meritsVirtues
  console.info("根据Id更新功德主名，_id: ",_id,"功德主名称（meritsVirtues）：",meriName)
  const updateResult = db.collection("kindPersonInfo").doc(_id).update( { data: { meritsVirtues: meriName }})
  return updateResult
}
//更据手机号更新角色名称
const updateRoleByMobile = async (event) => {
  const mobile = event.mobile
  const role = event.role
  console.info('根据手机号更新角色，手机号(mobile)为:',mobile,'， 角色为：',role)
  const updResult = await db.collection("kindPersonInfo").where({mobile: mobile}).update({data: { roles: role}})
  return updResult
}
//更据ID重置角色名称
const restRolesById = async (event) => {
  const _id = event._id
  const updResult = await db.collection('kindPersonInfo').doc(_id).update({data: {roles: '普通用户'}})
  return updResult
}
//更新名字
const updateFullNameById = async (event) => {
  const _id = event._id
  const fullName = event.fullName
  const updResult = await db.collection('kindPersonInfo').doc(_id).update({data: {fullName: fullName}})
  console.log('更新名字结果为：',updResult)
  return updResult
}
const main = async (event) => {
  const reqType = event.reqType
  switch(reqType){
    case "updateKindNameByMobile":
      return await updateMeriNameById(event)
    case "updateRoleByMobile":
      return await updateRoleByMobile(event)
    case "restRolesById":
      return await restRolesById(event)
    case 'updateFullNameById':
      return await updateFullNameById(event)
    default:
      return {data: undefined, errMsg: '请求类型不存在.'}
  }
}
module.exports = { main }