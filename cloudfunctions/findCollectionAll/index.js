// 云函数入口文件
const opt_kindPersonInfo = require("./src/opt_kindPersonInfo")
const opt_freeCaptiveDetails = require('./src/opt_freeCaptiveDetails')
const opt_kindPersonList = require("./src/opt_kindPersonList")
const opt_paymentDetails = require('./src/opt_paymentDetails')

/**云函数入口函数，函数说明，根据传入日期查询功德主放生名单，在首页表格数据 
 * 输入参数一：collectionName: { type: string, 可空: 是, 描述: '集合名称', 默认值： 'freeCaptiveDetails'}
 * 输入参数二：dat: { type: String, 可空: 是, description: '日期名称', 默认值： nowDate}
 * */ 
exports.main = async (event, context) => {

  const collectionName = event.collectionName

  switch(collectionName){
    case 'freeCaptiveDetails':
      return await opt_freeCaptiveDetails.main(event)
    case 'kindPersonList':
      return await opt_kindPersonList.main(event)
    case 'kindPersonInfo':
      return await opt_kindPersonInfo.main(event)  
    case 'paymentDetails':
      return await opt_paymentDetails.main(event)
    default:
      console.error("集合名称不存在,输入的集合名称为：",collectionName)
      return {data: undefined, errMsg: '集合名称不存在'}
  }
}