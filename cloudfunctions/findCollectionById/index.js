const personInfo = require('./src/find_kindPersonInfo')
const paymengDetails = require('./src/find_paymentDetails')
// 云函数入口函数
exports.main = async (event, context) => {

  const collectionName = event.collectionName
  
  switch(collectionName) {
    case 'kindPersonInfo':
      return await personInfo.main(event)
    case 'freeCaptiveDetails':
      return ;
    case 'paymentDetails':
      return await paymengDetails.main(event)
    default:
      console.error("集合不存在")
      return {data: undefined, errMsg: "集合不存在."}
  }
}