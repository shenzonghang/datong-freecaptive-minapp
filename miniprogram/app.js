//app.js
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({ 
        env: 'evn-cloud-a-4gf1q6iy9df62d73', //cloud1环境
        traceUser: true,
      })
    }
    const phone = wx.getStorageSync('user_phoneNumber')
    const role = wx.getStorageSync('user_role')// 
    const meritsVirtues = wx.getStorageSync('user_meritsVirtues')
    const kindPeopleId = wx.getStorageSync('user_kindPeopleId')
    const session_key = wx.getStorageSync('user_session_key')
    const fullName = wx.getStorageSync('user_fullName')

    console.log('角色为: ', role)
    console.log('手机号号为：',phone)
    console.log("功德主名为：",meritsVirtues)
    console.log('session_key的值为：',session_key)
    
    this.globalData = {
      userInfo: {},
      hasUserInfo: phone != null && phone != "",
      userRole: (role != "") ? role: '普通用户' ,
      phoneNumber: phone,
      meritsVirtues: meritsVirtues == "" ? "" : meritsVirtues,
      kindPeopleId: kindPeopleId,
      session_key: session_key == '' ? '' : session_key,
      fullName: fullName
    }
  }
})
