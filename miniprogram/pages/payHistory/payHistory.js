/**用户支付历史 */
const utils = require('../../utils/util.js')
const app = getApp()
Page({

  data: {
    date: undefined,
    placeholderStr: '请选择日期.',
    loading: false,
    columon: [
      { prop: 'chargeTime',width: 120, label: '时间' },
      { prop: 'chargeBeforeBalance',width: 150,label: '充前额' },
      { prop: 'chargeAmount',width: 150,label: '充值额' },
      { prop: 'chargeAfterBalance',width: 150,label: '充后额' },
      { prop: 'chargeMobile',width: 180, label: '手机号',color: '#55C355'},
    ],
    footer: [
      { prop: 'chargeTime', width: 120, label: '总计' },
      { prop: 'chargeBeforeBalance', width: 150, label: '' },
      { prop: 'chargeAmount', width: 150, label: '' },
      { prop: 'chargeAfterBalance', width: 150, label: '' },
    ], 
    tableData: []
  },

  onLoad: function (options) {
    const dateStr = utils.formatDateOrTime(new Date,"yyyy-MM-dd")
    this.setData({endDate: dateStr})
  },

  onShow: function () {
    this.initTableData(undefined)
  },
 
  bindDateChange: function(e){
    const dateStr = e.detail.value
    this.setData({date: dateStr})
    this.initTableData(dateStr)
  },

  initTableData(dat){
    this.setData({ loading: true })
    const that = this
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: {collectionName: 'paymentDetails', date: dat, reqType: 'findManyByDate'},
      success: function(res){
        console.log('支付明细：',res)
        const result = res.result
        if(!result.data){
          that.setData({tableData: [], loading: false})
          return;
         }
        that.setData({ loading: false })
        const tabData = res.result.data
        if(tabData==undefined || tabData == null) return ;
        let sumChargeAmount = 0
        for(let i=0; i < tabData.length; i++){
          sumChargeAmount = Number(tabData[i].chargeAmount) + sumChargeAmount
          let date = tabData[i].chargeDate
          date = date.replace(/-/,'')
          const dateTime = date + ' ' + tabData[i].chargeTime
          tabData[i].chargeDateTime = dateTime
          const mobile = tabData[i].chargeMobile
          const len = mobile.length
          tabData[i].chargeMobile = mobile.substring(0,3) + '***' + mobile.substring(len - 4, len)
        }
        that.setData({
          'footer[2].label': Number(sumChargeAmount).toFixed(2),
          'footer[3].label': "日期： " + tabData[0].chargeDate,
          tableData: tabData
        })
      },
      fail: res => {
        // that.showToast()
        that.setData({tabData: [], loading: false })
      }
    })
  },
})