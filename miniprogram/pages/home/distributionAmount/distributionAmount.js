// pages/home/distributionAmount/distributionAmount.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show: {type: Boolean, value: false},
    totalFreeAmount: {type: Number, value: 0}
  },

  /**
   * 组件的初始数据
   */
  data: {
    buttons: [{text: '取消'}, {text: '确定'}],
  },

  /**
   * 组件的方法列表
   */
  methods: {
	  btnClickHandle: function(e){
      const index = e.detail.index
      if(index == 0){//取消
        this.cancelBtnClick()
      }else if(index === 1){//确定
        this.confirmBtnClick()
      }else{
        console.error('类型不存在.')
      }
    },
    confirmBtnClick(e){
      console.log('确定按钮被点击.totalFreeAmount: ',this.data.totalFreeAmount)
      const that = this
      wx.cloud.callFunction({
        name: "updateCollectionById",
        data: { totalFreeAmount: this.data.totalFreeAmount, collectionName: 'freeCaptiveDetails', reqType: 'autoDistributeFreeAmount'},
        success: function(res){
          console.log('分页返回结果为：',res)
          const result = res.result

        },
        fail: res => {
          that.setData({})
        }
      })
      this.cancelBtnClick()
    },
    cancelBtnClick(e){
      console.log('取消按钮被点击.')
      this.setData({show: false})
    }
  }
})
