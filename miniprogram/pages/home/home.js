const utils = require('../../utils/util.js')
const app = getApp()
Page({
  data: {
    date: undefined,
    placeholderStr: "请选择日期~",
    endDate: '',
    freeAmountBtnLoading: false,
    loading: true,
    dataModal: Object,
    balance: 0,
    selectedId: 2,
    stripe: true,
    outBorder: true,
    tableData: [],
    msg: '暂无数据',
    isShowModal: false,
    showDistrAmouDialog: false,
    userRole: '',
    columon: [
      { prop: 'meritsVirtues',width: 450, label: '名称',color: '#55C355'},
      { prop: 'freeAmount',width: 150,label: '放额' },
      { prop: 'balance',width: 150, label: '余额' },
    ],
    footer: [
      { prop: 'meritsVirtues', width: 450, label: '总计',color: '#55C355'},
      { prop: 'freeAmount', width: 150, label: '' },
      { prop: 'balance', width: 150, label: '' },
    ], 
  },
 
  onLoad: function (options) {
    const dateStr = utils.formatDateOrTime(new Date,"yyyy-MM-dd")
    this.setData({endDate: dateStr})
    console.log('全局变量userInfo的值：',)
  },
  onShow: function(e){
    this.setData({pageSize: 12, pageNumber: 1})
    this.pageInit()
    const userRole = app.globalData.userRole || wx.getStorageSync('user_role')
    console.info('userRole: ',userRole)
    this.setData({userRole: userRole})
  },
  onShareAppMessage(res) {
    console.log('res: ',res)
    return { title: '随喜放生', path: '/pages/home/home'}
  },
  /**点击行事件 */
  on_row_click: function(e) {
    this.setSelectedBackground(e)
    this.setData({ tableData: this.data.tableData})
  },
  /**长按行事件 */
  onLongPress: function(e){
    //设置选中背景颜色
    this.setSelectedBackground(e)

    this.showModal(e)
  },
  /** 设置所选行的背景颜色 */
  setSelectedBackground: function(e){
    const id = e.detail.currentTarget.id
    const selectId = this.selectedId
    if(selectId != null && selectId != undefined){
      const key = 'tableData[' + selectId + '].isSelect'
      this.setData({ [key]: false })
    }
    const key = 'tableData[' + id + '].isSelect'
    this.setData({ [key]: true})
    this.selectedId = id
  },
  //表格初始化
  pageInit: function(dat){
    this.setData({ loading: true })
    const that = this
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: {dat: dat, collectionName: 'freeCaptiveDetails', reqType: 'findAll'},
      success: function(res){
        console.log('分页返回结果为：',res)
        const result = res.result
        if(result.code == undefined || result.code != 0){
          that.setData({tableData: [], loading: false})
          return;
         }
        that.setData({
          tableData: result.data.list, 
          loading: false,
          'footer[0].label': result.data.list[0].freeDate,
          'footer[1].label': Number(result.data.sumFreeAmount).toFixed(2),
          'footer[2].label': Number(result.data.sumBalance).toFixed(2),
        })
        console.log('查询放生明细表成功.')
      },
      fail: res => {
        that.setData({tabData: [], loading: false })
      }
    })
  },
  /**显示模态框,初始化模态框数据 */
  showModal: function(e) {
    this.data.dataModal = {}
    const it = e.detail.currentTarget.dataset.it
    const userRole = app.globalData.userRole

    const meritIn = it.meritsVirtues
    const meritGlobal = app.globalData.meritsVirtues

    if(userRole != '救生员' && userRole != '管理员' && (meritIn != meritGlobal && userRole == '普通用户') ){
      console.warn('用户无权修改，角色为：' + userRole)
      console.info('选定的功德主名称为：',meritIn,'全局功德主名称为：',meritGlobal)
      return;
    }
    //本次放生后余额
    it.nativeFreeBalance = Number(it.balance - it.freeAmount).toFixed(2)
    this.setData({ isShowModal: true, dataModal: it })
  },
  bindDateChange: function(e){
    const dateStr = e.detail.value
    this.setData({date: dateStr})
    this.pageInit(dateStr)
  },
  refreshModal(){
    this.pageInit()
  },
  //生成按钮点击事件，填充放生额，放生额生成规则：
  optBarBtnClick: function(e){
    const userRole = this.data.userRole
    if(userRole=='救生员' || userRole === '管理员'){
      this.setData({showDistrAmouDialog: true})
    }
  },
})