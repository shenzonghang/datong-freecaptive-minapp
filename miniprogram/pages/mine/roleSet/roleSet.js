// miniprogram/pages/mine/adminSet.js
/**管理员设置 */
Page({
  data: {
    adminList: [],
    rescueList: [],
    loading: false,
    selectedCellClassify: '', //是否选中列表标志位
    lastTapTime: 0,
    showDialog: false,
    titleDialog: '',
    deleteIdx: -1, //列表重置按钮标志位
    startTouchX: '',
    startMobile: '',
  },
  onLoad: function (options) {
    this.getInitData()
  },
  getInitData(){
    const that = this
    if(this.data.adminList.length == 0 && this.data.rescueList.length==0){
      this.setData({loading: true})
    }
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: { collectionName: 'kindPersonList', sign: "getAdminAndResuce"},
      success: function(response){
        const result = response.result
        let adminList = result.adminList
        let rescueList = result.rescueList
        console.log('获取管理员和救生员成功，管理员列表为：',adminList,"\n救生员列表为：", rescueList)
        that.setData({adminList: adminList, rescueList: rescueList})
      },
      fail: res => {
        console.error('操作失败.')
        return null
      },
      complete: () => {
        that.setData({loading: false})
      }
    })
  },
  onClickCellHandle: function(e){
    var classify = e.currentTarget.dataset.classify
    this.setData({selectedCellClassify: classify})
    var curTime = e.timeStamp
    var lastTime = e.currentTarget.dataset.time
    if (curTime - lastTime > 0) {
      if (curTime - lastTime < 300) {//是双击事件
        const title = e.currentTarget.dataset.type
        this.setData({showDialog: true, titleDialog: title, })
      }
    }
    this.setData({ lastTapTime: curTime})
  },
  //开始触摸
  startTouch: function(e){
    console.log('开始触摸事件被触发...e: ', e)
    const touchX = e.changedTouches[0].clientX
    const mobile = e.currentTarget.dataset.mobile
    this.setData({startTouchX: touchX, startMobile: mobile})
  },
  //触摸结束，判断是否是触摸事件
  endTouch: function(e){
    console.log('触摸事件结束...e: ', e)
    const endTouchX = e.changedTouches[0].clientX
    const startTouchX = this.data.startTouchX
    const endMobile = e.currentTarget.dataset.mobile
    const startMobile = this.data.startMobile
    const slideDis = startTouchX - endTouchX
    if(endMobile === startMobile && startTouchX > endTouchX && slideDis > 30){
      console.log('被滑动')
      this.setData({deleteIdx: endMobile})
    }
  },
  restRole: function(e){
    const that = this
    const titleType = e.currentTarget.dataset.title
    const isRescueListOnlyOne = titleType == '救生员' && this.data.rescueList.length == 1
    const isAdminListOnlyOne = titleType == '管理员' && this.data.adminList.length == 1
    if(isRescueListOnlyOne || isAdminListOnlyOne){
      wx.showToast({title: "至少保留一个", icon: "error", duration: 2000})
      return;
    }
    const _id = e.currentTarget.dataset.id
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { collectionName: 'kindPersonInfo', reqType: "restRolesById", _id: _id},
      success: function(response){
        const result = response.result
        if(result != null && result.stats.updated == 1 ){
          console.log('重置成功',response)
          that.getInitData()
          return ;
        }
        wx.showToast({title: "重置失败", icon: "error", duration: 2000})
      },
      fail: res => {
        console.error('操作失败.')
        return null
      }
    })
    this.setData({selectedCellClassify: '', deleteIdx: ''})
  }
})