// pages/mine/adminSet/addRole/addRole.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: { type: String, value: '' },
    show: { type: Boolean, value: false },
    mobile: { type: String, value: '' }
  },

  /**
   * 组件的初始数据
   */
  data: {
    title: '',
    show: false,
    buttons: [{text: '取消'}, {text: '添加'}],
    fullName: '',
    mobile: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getFullName(mobile){
      const reg = /^1\d{10}$/
      if(mobile.length != 11 || !(reg.test(mobile))){
        wx.showToast({title: "输入格式错误", icon: "icon-box-img", duration: 2000})
        return;
      }
      const that = this
      this.setData({loadingDialog: true, mobile: mobile})
      wx.cloud.callFunction({
        name: "findCollectionById",
        data: { collectionName: 'kindPersonInfo', mobile: mobile},
        success: function(response){
          const data = response.result.data
          if(data == '' || data == undefined || data == null){
            wx.showToast({title: "手机号不存在", icon: "icon-box-img", duration: 2000})
            return
          }
          let name = data[0].fullName
          if(name=='' || name == undefined || name == null){
            name = "<空>"
          }
          that.setData({fullName: name})
        },
        fail: res => {
          console.error('操作失败.')
          return null
        }
      })
    },
     //失去焦点时触发
    inputBlur: function(e){
      const mobile = e.detail.value
      if(mobile.length == 0){ return; }
      this.getFullName(mobile)
    },
    //点击完成时触发
    inputDone: function(e){
      const mobile = e.detail.value
      this.getFullName(mobile)
    },
    //点击确定或取消时触发
    tapDialogButton: function(e){
      console.log('确定或取消按钮..')
      const that = this
      const index = e.detail.index
      if(index == 0){
        this.setData({show: false})  
        return;
      }
      //点击确定按钮
      if(index == 1){
        const mobile = this.data.mobile
        const titleDialog = this.data.title
        if(mobile.length != 11 || !((/^1\d{10}$/).test(mobile))){
          wx.showToast({title: "输入格式错误", icon: "icon-box-img", duration: 2000})
          return;
        }
        const roleIn = (titleDialog == "新增救生员") ? '救生员' : '管理员'
        wx.cloud.callFunction({
          name: "updateCollectionById",
          data: { collectionName: 'kindPersonInfo', mobile: mobile, reqType: 'updateRoleByMobile', role: roleIn},
          success: function(response){
            console.log('response:',response)
            const stats = response.result.stats
            if(stats != null && stats.updated == 1){
              wx.showToast({title: "添加成功", icon: "icon-box-img", duration: 2000})
              // that.getInitData()
              that.triggerEvent("refreshList",{},{})
              that.setData({show: false})  
              return
            }
            wx.showToast({title: "添加失败", icon: "error", duration: 2000})
          },
          fail: res => {
            console.error('操作失败.')
            return null
          }
        })
      }
    }, 
  },
  observers: {
    'show': function(value){
      console.log('value: ',value)
      if(value){
        console.log("重置手机号和名字")
        this.setData({fullName: '', mobile: ''})
      }
    }
  }
})
