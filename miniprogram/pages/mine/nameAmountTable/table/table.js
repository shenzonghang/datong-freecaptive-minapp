// pages/mine/nameAmountTable/table.js
Component({
  properties: {
    data: Array,
    sumFreeAmount: Number,
    loading: Boolean,
    touchStartX: Number,
    touchStartY: Number
  },
  data: {
    data: []
  },
  methods: {
    //开始触屏
    touchStart: function(e){
      console.log('touchStart:',e)
      const touchStartX = e.changedTouches[0].clientX
      const touchStartY = e.changedTouches[0].clientY
      this.setData({touchStartX: touchStartX, touchStartY: touchStartY})
    },
    //触屏结束
    touchEnd:function(e){
      const touchEndX = e.changedTouches[0].clientX
      const touchEndY = e.changedTouches[0].clientY
      const touchStartX = this.data.touchStartX
      const touchStartY = this.data.touchStartY
      //左滑，下一页
      if((touchStartX - touchEndX) > 40 && Math.abs(touchEndY - touchStartY) < 120){
        this.nextPage()
      }
      //右滑，上一页
      if((touchEndX - touchStartX ) > 30 && Math.abs(touchEndY - touchStartY) < 120){
        this.previousPage()
      }
    },
    nextPage: function(e){
      this.triggerEvent('_next_page', e ,{})
    },
    previousPage: function(e){
      this.triggerEvent('_previous_page', e ,{})
    }
  }
})
