// miniprogram/pages/mine/nameAmountTable/nameAmountTable.js
const utils = require("../../../utils/util")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tableData: [],
    loading: false,
    sumFreeAmount: 0,
    pageSize: 10,
    pageNumber: 1,
    totalCount: 0,
    availableHeight: 0,
    gridNameWidth: 0,
    skip: 0,
    pageType: ''
  },

  getAvailableHeight: async function(e){
    const query = wx.createSelectorQuery();
    query.select('#nameAmountTable_page').boundingClientRect()
    const height = await new Promise(resolve=>{
      query.exec((res) => {
      let height = res[0].height
      height = height - 60 - 48
      resolve(height)
    })})
    return height
  },
  settingHeightAndWidth: async function(e){
    const availHe = await this.getAvailableHeight()
    console.log('availHe: ',availHe)
    const screenWidth = wx.getSystemInfoSync().windowWidth//屏幕宽度
    // 表格名称宽度设置为600rpx，屏幕总宽度750rpx， 根据数学比例公式有：750：600 = 15： 12
    const gridNameWidth = screenWidth * 12 / 15 
    console.log('屏幕宽度为：',screenWidth,',表格名称列的宽度为：', gridNameWidth,',可用高度为：',availHe)
    this.setData({gridNameWidth: gridNameWidth, availableHeight: availHe})
  },
  onLoad: function (options) {
    this.pageInit()
  },
  pageInit: async function(){
    await this.settingHeightAndWidth()//获取宽度和高度
    console.log('开始请求云函数参数为：skip:',this.data.skip,'pageSize: ',this.data.pageSize)
    this.setData({ loading: true })
    const that = this
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: { 
        collectionName: 'freeCaptiveDetails',
        reqType: 'findPage', 
        skip: this.data.skip, 
        pageSize: this.data.pageSize,
        pageType: this.data.pageType,
        availableHeight: this.data.availableHeight,
        gridNameWidth: this.data.gridNameWidth,
        totalCount: this.data.totalCount
      },
      success: async (res) => {
        console.log("查询方生明细结果为：",res)
        const result = res.result
        if(!result.data){
          that.setData({tableData: [], loading: false})
          return;
        }
        that.setData({
          totalCount: result.data.totalCount,
          tableData: result.data.list, 
          loading: false,
          sumFreeAmount: result.data.sumFreeAmount,
          skip: result.data.skip,
          pageSize: result.data.pageSize,
        })
        console.log('sumFreeAmount: ',that.data)
      },
      fail: res => {
        // that.showToast()
        that.setData({tabData: [], loading: false })
      }
    })
  },
  getTotalPageNumber: function(totalCount, pageSize){
    const k = Number((totalCount / pageSize).toFixed(2).split(".")[0])
    const b = Number((totalCount % pageSize == 0 ) ? 0 : 1)
    return k + b
  },
  nextPageClickHandle: function(e){
    const _skip = Number(this.data.skip)
    const _pageSize = Number(this.data.pageSize)
    const _totalCount = Number(this.data.totalCount)
    if(_totalCount > _skip + _pageSize){
      this.setData({pageType: 'nextPage', skip: _skip + _pageSize})
      console.log('canshu wei : ',this.data)
      this.pageInit()
    }else{
      wx.showToast({title: "已是末页", icon: "success", duration: 2000})
    }
  },
  previousPageClickHandle: function(e){
    const _skip = this.data.skip
    if(_skip > 0 ){
      this.setData({pageType: 'previousPage'})
      this.pageInit()
    }else{
      wx.showToast({title: "已是首页", icon: "success", duration: 2000})
    }
  },
})