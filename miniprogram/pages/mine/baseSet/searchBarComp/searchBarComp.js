// pages/mine/kindPeopSet/searchBar.js
/**更改功德主名称-功德主搜索页 */
Component({
  /** 组件的属性列表*/
  properties: {
    inputShowed: {type: Boolean, value: false},
    inputVal: { type: String, value: '' },
    list: Array,
  },
  /** 组件的初始数据*/
  data: {
    selectedBackground: "#EBEEF5",
    selectedId: ''
  },
  /** 组件的方法列表 */
  methods: {
    bindInput: function(e) {
      this.setData({inputVal: e.detail.value})
      this.triggerEvent('on_bind_input', e, {})
    },
    bindfocus: function(e) {
      this.setData({inputShowed: true})
      this.triggerEvent('on_bind_focus', e, {})
    },
    clearInput: function(e){
      this.setData({ inputShowed: false, list: [], inputVal: '' })
    },
    onKindNameSelectedHandle: function(e){
      const item = e.currentTarget.dataset.item
      var classify = e.currentTarget.dataset.classify;
      this.setData({selectedId: classify, inputShowed: false, inputVal: item.meritsVirtues})
      this.triggerEvent('on_selected',item,{})
    }
  },
  observers: {
    'list': function(list) {
     
    },
  },
})
