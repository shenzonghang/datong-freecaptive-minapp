/** 功德主设置页 */
const app = getApp()
Page({
  data: {
    mobile: null,
    isEdit: false,
    selected: false,
    searchResultList: [],
    searchInputValue: '',
    fullName: '',
    newFullName: '',
  },

  onLoad: function (options) {
    console.log('全局变量（app）的值为：',app.globalData)
    this.setData({
      mobile: app.globalData.phoneNumber,
      searchInputValue: app.globalData.meritsVirtues,
      fullName: app.globalData.fullName,
      newFullName: app.globalData.fullName
    })
  },
  //输入文字后自动搜索
  searchStart: function(value) {
    const that = this
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: { collectionName: 'kindPersonList', searchInfo: value == '' ? '' : value },
      success: function(res){
        const result = res.result
        const data = result.data
        if(data == null || data == undefined){
          console.error('云函数返回结果为空.')
          return ;
        }
        that.setData({searchResultList: data})
      },
      fail: res => {
        console.error('请求云函数失败.')
        return null
      }
    })
  },
  bindModify: function(e){
    this.setData({isEdit: true})
  },
  searchInput: function(e){
    const inputContent = e.detail.detail.value
    this.searchStart(inputContent)
  },
  searchFocus: function(e){
    this.searchStart('')
  },
  onSelectedHandle: function(e){
    const item = e.detail
    const _id = app.globalData.kindPeopleId == "" ? wx.getStorageSync('user_kindPeopleId'): app.globalData.kindPeopleId
    const meritsVirtues = item.meritsVirtues
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { id: _id, meritsVirtues: meritsVirtues, reqType: 'updateKindNameByMobile', collectionName: 'kindPersonInfo' },
      success: function(res){
        console.log("res: ",res)
        const result = res.result
        const updatedCount = result.stats.updated
        if(updatedCount > 0){
          wx.showToast({title:"更改成功", icon: "success", duration: 1000})
          wx.setStorage({ data: meritsVirtues, key: 'user_meritsVirtues'})
          app.globalData.meritsVirtues = meritsVirtues
          return
        }
        wx.showToast({title:"更改失败", icon: "error", duration: 2000})
      },
      fail: res => {
        wx.showToast({title:"更改失败", icon: "error", duration: 2000})
      }
    })
    this.setData({ isEdit: false, searchInputValue: meritsVirtues })
  },
  fullNameInputBlur: function(e){
    const _id = app.globalData.kindPeopleId || wx.getStorageSync('user_kindPeopleId')
    const newFullName = e.detail.value
    console.info('请求参数为：_id:' + _id + "，名字为：" + fullName)
    if(newFullName == '' || newFullName == undefined || _id == '' || _id == undefined){
      console.error('名称和id不能为空.')
      return;
    }
    const fullName = this.data.fullName
    if(fullName == newFullName){
      console.info('新姓名和老姓名相等，无需更新.')
      return;
    }
    const that = this
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { _id: _id, fullName: newFullName, reqType: 'updateFullNameById', collectionName: 'kindPersonInfo' },
      success: function(res){
        console.log("更新姓名结果为res: ",res)
        if(res.result.stats.updated == 1){
          wx.showToast({title: "更改姓名成功.", icon: "success", duration: 2000})
          console.info('更改姓名成功.')
          that.setData({fullName: newFullName, newFullName: newFullName})
          wx.setStorage({ data: newFullName, key: 'user_fullName'})
          app.globalData.fullName = newFullName
          return;
        }
        wx.showToast({title: "更改失败", icon: "error", duration: 2000})
      },
      fail: res => {
        wx.showToast({title:"更改失败", icon: "error", duration: 2000})
      }
    })
  }
})