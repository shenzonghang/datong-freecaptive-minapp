/**功德主信息维护 */
Page({

  data: {
    searchResultList: [],
    inputVal: '',
    textareaValue: ''
  },
  onShow: function(e){
    this.searchStart('')
  },
  bindInput: function(e){
    const valu = e.detail.value
    this.searchStart(valu)
  },
  bindfocus: function(e){
    this.searchStart('')
  },
  searchStart: function(value) {
    const that = this
    wx.cloud.callFunction({
      name: "findCollectionAll",
      data: { collectionName: 'kindPersonList', searchInfo: value == '' ? '' : value },
      success: function(res){
        const result = res.result
        const data = result.data
        if(data == null || data == undefined){
          that.setData({searchResultList: []})
          console.info('云函数返回结果为空.')
          return ;
        }
        that.setData({searchResultList: data,editInputValue: ''})
      },
      fail: res => {
        console.error('请求云函数失败.')
        return null
      }
    })
  },
  //清除按钮
  deleteClickHandle: function(e){
    const item = e.currentTarget.dataset.item
    const _id = item._id
    const that = this
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { _id: _id, collectionName: "kindPersonList", reqType: 'deleteById' },
      success: function(res){
        console.log('删除结果为：',res)
        if(res.result.code == 0){
          that.searchStart('')
          return;
        }
        console.error('操作失败.')
        wx.showToast({title: res.result.errMsg,icon: "error",duration: 2000})
      },
      fail: res => {
        wx.showToast({title:"删除失败",icon: "error",duration: 2000})
      }
    })
  },
  //新增按钮事件
  addInfoClickHandle: function(e){
    wx.navigateTo({
      url: './addKindMaster/addKindMaster',
    })
  },
  updateClickHandle: function(e){
    console.log('e: ',e)
    const item = e.currentTarget.dataset.item
    const itemStr = JSON.stringify(item)
    wx.navigateTo({
      url: './updateKindMaster/updateMasterMaster?item= ' + itemStr,
    })
  }
})