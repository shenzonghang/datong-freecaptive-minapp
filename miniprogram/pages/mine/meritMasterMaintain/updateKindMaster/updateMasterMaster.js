// miniprogram/pages/mine/meritMasterMaintain/updateKindMaster/updateMasterMaster.js
Page({
  data: {
    _id: '',
    textareaValue: ''
  },
  onLoad: function (options) {
    console.log('options: ',options)
    const item_Str = options.item
    const item = JSON.parse(item_Str)
    const _id = item._id
    const merit = item.meritsVirtues
    this.setData({_id: _id, textareaValue: merit})
  },
  bindConfirmClickHandle: function(e){
    console.log('e: ',e)
    const _id = e.currentTarget.dataset._id
    const merit = e.detail.value
    const that = this
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { _id: _id, meritsVirtues: merit, collectionName: "kindPersonList", reqType: 'updatedMeritById' },
      success: function(res){
        console.log('更新结果为：',res)
        if(res.result.code == '0000'){
          setTimeout(() => { wx.navigateBack() }, 1400)
          wx.showToast({ title:"更新成功", icon: "success", duration: 1400 })
          console.log('更新成功')
          return;
        }
        console.error('操作失败.')
        wx.showToast({title: res.result.errMsg,icon: "error",duration: 2000})
      },
      fail: res => {
        wx.showToast({title:"更新失败",icon: "error",duration: 2000})
      }
    })
  }
})