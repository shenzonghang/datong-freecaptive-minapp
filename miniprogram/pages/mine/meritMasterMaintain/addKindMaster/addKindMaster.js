// miniprogram/pages/mine/meritMasterMaintain/addKindMaster/addKindMaster.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    textareaValue: ''
  },
  //点击完成
  bindConfirmClickHandle: function(e){
    console.log('确定按钮被点击, e:',e.detail.value)
    const that = this
    that.setData({loading: true})
    const merit = e.detail.value
    if(merit == ''){
      wx.showToast({title:"名称不能为空",icon: "warn",duration: 1500})
      that.setData({loading: false})
      return;
    }
    wx.cloud.callFunction({
      name: "updateCollectionById",
      data: { meritsVirtues: merit, collectionName: "kindPersonList", reqType: 'addInfo' },
      success: function(res){
        console.log('请求成功，返回值为：',res)
        that.setData({loading: false})
        if(res.result._id){
          setTimeout(() => { wx.navigateBack() }, 1600)
          wx.showToast({ title:"新增成功", icon: "success", duration: 1500 })
          return;
        }
        if(res.result.errMsg == '功德主名已存在，不能重复添加.'){
          wx.showToast({title:"名称已存在", icon: "error", duration: 1500})
          console.error('功德主名已存在，不能重复添加.')
          return ;
        }
        wx.showToast({title: res.result.errMsg, icon: "error", duration: 1500})
      },
      fail: res => {
        wx.showToast({title:"新增失败",icon: "error",duration: 2000})
        that.setData({loading: false})
      }
    })
  }
})