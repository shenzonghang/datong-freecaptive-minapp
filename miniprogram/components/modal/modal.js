/* components/modal/modal.js */
Component({
  /** 组件的属性列表 */
  properties: {
    show: { type: Boolean , value: false },
    height: { type: String, value: '' },
    title: { type: String, value: '' },
    showCancel: { type: Boolean, value: true },
    cancelText: { type: String, value: '取消' },
    cancelColor: {type: String, value: '' },
    confirmText: { type: String, value: '' },
    confirmColor: { type: String, value: '' },
    clickClose: { type: Boolean, value: true }
  },

  /** 组件的初始数据*/
  data: {
    show: false,
    height: '50%',
  },

  /** 组件的方法列表 */
  methods: {
    cancelClick(e){
      this.triggerEvent('cancelClick', e, {})
    },
    confirmClick(e){
      this.triggerEvent('confirmClick', e, {})
    }
  }
})
