Component({
  /** 外部样式类*/
  externalClasses: ['header-row-class-name', 'row-class-name', 'cell-class-name'], 
  /** 组件样式隔离 */
  options: {
    styleIsolation: "isolated", 
    multipleSlots: true, // 支持多个slot
  },
  /** 组件的属性列表*/
  properties: {
    loading: { type: Boolean, value: true },
    endTime: { type: Date, value: null },
    startTime: { type: Date, value: null },
    data: { type: Array, value: [] },
    footer: { type: Array, value:[] },
    headers: { type: Array, value: [] },
    // table的高度, 溢出可滚动
    height: { type: String, value: 'auto'},
    pressLongBackground: {type: String, value: ''},
    width: { type: Number || String, value: '100%' },
    // 单元格的宽度
    tdWidth: { type: Number, value: 35 },
    // 固定表头 thead达到Header的位置时就应该被fixed了
    offsetTop: { type: Number, value: 150 },
    // 是否带有纵向边框
    stripe: { type: Boolean, value: true },
    // 是否带有纵向边框
    border: { type: Boolean, value: true }, 
    msg: { type: String, value: '暂无数据~' }
  },
  /** 组件的初始数据 */
  data: {
    scrolWidth: '100%',
  },
  /** 组件的监听属性 */
  observers: {
    // 在 numberA 或者 numberB 被设置时，执行这个函数
    'headers': function (headers) {
      const reducer = (accumulator, currentValue) => {
        return accumulator + Number(currentValue.width)
      };
      const scrolWidth = headers.reduce(reducer, 0)
      this.setData({
        scrolWidth: scrolWidth
      })
    }
  },
  /** 组件的方法列表 */
  methods: {
    onRowClick(e) {
      if(this.endTime - this.startTime < 400){
        this.triggerEvent('on_row_click', e, {})
      }
    },
    //长按事件
    longPress(e){
      this.triggerEvent('on_long_press', e, {})
    },
    touchStart(e){
      console.log('开始触摸：',e)
      this.startTime = e.timeStamp
    },
    touchEnd(e){
      console.log('结束触摸：',e)
      this.endTime = e.timeStamp
    }
  }
})