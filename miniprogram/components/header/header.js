// components/header/header.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    nbTitle: { type: String, value: '资金监管' },
    nbLoading: { type: Boolean, value: false },
    nbFrontColor: { type: String, value: '#ffffff' },
    nbBackgroundColor: { value: '#000000', type: String },
  },
  /**
   * 组件的初始数据
   */
  data: {
    nbFrontColor: '#000000',
    nbBackgroundColor: '#ffffff',
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
