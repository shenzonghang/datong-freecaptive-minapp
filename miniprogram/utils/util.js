const formatDateOrTime = async (date, rule) => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  switch(rule){
    case "yyyyMMddHHmmss":
      return `${[year, month, day, hour, minute, second].map(formatNumber).join('')}`
    case "yyyy-MM-dd":
      return `${[year, month, day].map(formatNumber).join('-')}`
    case "yyyy-MM-dd HH:mm:ss":
      return `${[year, month, day].map(formatNumber).join('-')} ${[hour, minute, second].map(formatNumber).join(':')}`
    case "HH:mm:ss":
      return ` ${[hour, minute, second].map(formatNumber).join(':')}`
    default:
      return null;
  }
}
// 支持es4语法
const numberToFixed = (value,count) => {
  let number = Number(value)
  return number.toFixed(count)
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const getRandomNumber = (len) => {
  len = len || 32
  const chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
  var maxPos = chars.length;
  let str = ''
  for (let i = 0; i < len; i++) {
    str += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  console.log('生成的随机字符串为：',str)
  return str
}
const formatterOrder = order => {
  order = new String(order)
  let str = '';
  for(let i=order.length; i < 5; i++){
    str = str + '0'
  }
  str = str + '' + order
  console.info('新生成的订单号为：',str)
  return str
}
module.exports = {
  formatDateOrTime,
  numberToFixed,
  getRandomNumber,
  formatterOrder
}
