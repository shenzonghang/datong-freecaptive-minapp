 const app = getApp()
 //登录是否有效
 const isValidSessionKey = async () => {
  return await new Promise(resolve => {
    return wx.checkSession({
      success () {
        resolve(true)
      },
      fail () {
        resolve(false)
      }
    })
  })
}
const getCodeBylogin = async () => {
  return await new Promise((resolve, reject)=>{
    wx.login({
      timeout: 60 * 1000,
      success: (res)=>{
        const code = res.code
        console.info('登录成功，获取的code为：',code)
        resolve(code)
      },
      fail:(res) => {
        console.log('登录失败。')
        resolve(undefined)
      }
    })
  })
}
const getSessionkeyByCode = async (code) => {
  return await new Promise( resolve =>{
    wx.cloud.callFunction({
      name: "encryOrDecryp",
      data: { reqType: 'getSessionKey', code: code },
      success: function(res){
        console.log('获取session_key结果为：', res)
        const sessionKey = res.result.session_key
        resolve(sessionKey)
      },
      fail: res => {
        wx.showToast({title:"登录失败",icon: "error",duration: 2000})
        resolve(undefined)
      }
    })
  }) 
}
module.exports = {
 isValidSessionKey,
 getCodeBylogin,
 getSessionkeyByCode
}