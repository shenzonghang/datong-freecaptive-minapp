# 小程序云数据库云函数——账务管理
## 支付
1. 调用微信支付接口充值。
## 名称列表
1. 显示当前余额，日期，当日支付金额等信息
## 用户充值历史
1. 显示充值的手机号，金额相关信息
## 登录
1. 使用手机号登录


![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192336_76d90ae0_1947453.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192440_90163f97_1947453.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192533_a0d903d3_1947453.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192607_d8333ba1_1947453.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192631_dc5f0da1_1947453.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/192656_baaaf3c0_1947453.png "屏幕截图.png")